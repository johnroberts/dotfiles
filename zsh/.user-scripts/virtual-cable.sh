#!/bin/bash

# From https://gist.github.com/makidoll/a8f75de9046350991c93b9bc1655e1d4

case $1 in
	list)
	pactl list short modules | grep " (Virtual Cable)"
	;;

	add)
	if [ -z ${2} ];
	then
		echo "./virtual-cable.sh add <name may have spaces>"
	else
		export SINK_NAME=vc-sink-$(tr -dc 0-9 </dev/urandom | head -c 6; echo '')
		export SOURCE_NAME=vc-source-$(tr -dc 0-9 </dev/urandom | head -c 6; echo '')
		export DESCRIPTION="${@:2} (Virtual Cable)"
		pactl load-module module-null-sink sink_name=$SINK_NAME sink_properties="'device.description=\"$DESCRIPTION\"'"
		pactl load-module module-remap-source source_name=$SOURCE_NAME master=$SINK_NAME.monitor source_properties="'device.description=\"$DESCRIPTION\"'"
	fi
	;;

	remove)
	if [ -z ${2} ];
	then
		echo "./virtual-cable.sh remove <id>"
	else
		pactl unload-module ${2}
	fi
	;;

	*)
	echo "./virtual-cable.sh <list,add,remove>"
	;;
esac