# Summary
My dotfiles, managed via this repo and [stow](https://www.gnu.org/software/stow/)

# Usage
Install `stow`, then clone this repo to a directory underneath `~` (such as `~/.dotfiles`). Within the cloned directory, run `stow appname` to create symlinks that manage `appname`'s config according to this repository's contents.

```
git clone git@gitlab.com:johnroberts/dotfiles.git ~/.dotfiles
cd ~/.dotfiles
stow git zsh ohmyzsh # and others (one folder per dotfile-controlled app)
```

# Repo Directory Structure
One dir per dotfile-managed application. Nested dirs/files under the per-app dir, mirroring the desired location under `$HOME`.

Stow has two useful properties enabling this:
- Upon executing `stow`, it creates symlinks one directory above the working directory
- `stow` respects the directory structure of each per-application directory. This enables e.g. symlinking `.config/foo/.foorc` of this repo to `$HOME/.config/foo/.foorc` when you execute `stow foo` from this repo's root directory.

# Inspiration
https://alexpearce.me/2016/02/managing-dotfiles-with-stow/

